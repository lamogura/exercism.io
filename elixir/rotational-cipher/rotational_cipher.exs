defmodule RotationalCipher do
  @doc """
  Given a plaintext and amount to shift by, return a rotated string.

  Example:
  iex> RotationalCipher.rotate("Attack at dawn", 13)
  "Nggnpx ng qnja"
  """
  
  @spec rotate(text :: String.t(), shift :: integer) :: String.t()
  def rotate(text, shift) do
    text
    |> String.to_charlist
    |> Enum.map(fn
        char when char < ?9 -> char
        char when char < ?Z -> ?A + Integer.mod(char - ?A + shift, 26)
        char                -> ?a + Integer.mod(char - ?a + shift, 26)
      end)
    |> to_string
  end
end

