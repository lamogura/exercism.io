defmodule SecretHandshake do
  @doc """
  Determine the actions of a secret handshake based on the binary
  representation of the given `code`.

  If the following bits are set, include the corresponding action in your list
  of commands, in order from lowest to highest.

  1 = wink
  10 = double blink
  100 = close your eyes
  1000 = jump

  10000 = Reverse the order of the operations in the secret handshake
  """
  use Bitwise
  
  @text_flags [
    {0b1, "wink"},
    {0b10, "double blink"},
    {0b100, "close your eyes"},
    {0b1000, "jump"},
  ]

  @reverse_flag 0b10000

  defp has_flag(flag, code), do: (flag &&& code) == flag

  @spec commands(code :: integer) :: list(String.t())
  def commands(code) do
    text_commands = @text_flags |> Enum.flat_map(fn {flag, str} ->
      case has_flag(flag, code) do
        true -> [str]
        false -> []
      end
    end)
    
    case has_flag(@reverse_flag, code) do
      true -> Enum.reverse(text_commands)
      false -> text_commands
    end
  end

end

